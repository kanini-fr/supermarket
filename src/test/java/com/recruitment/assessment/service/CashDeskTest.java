package com.recruitment.assessment.service;

import com.recruitment.assessment.model.Apple;
import com.recruitment.assessment.model.Item;
import com.recruitment.assessment.model.Orange;
import com.recruitment.assessment.model.WaterMelon;
import com.recruitment.assessment.offer.BuyOneGetOneFree;
import com.recruitment.assessment.offer.BuyTwoGetOneFree;
import com.recruitment.assessment.offer.BuyTwoGetTwoFree;
import com.recruitment.assessment.offer.NoOffer;
import org.junit.Test;

import static java.lang.Math.abs;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CashDeskTest {

    /**
     * For the comparison of double typed values. NB : Sometime Double.compare() does not work properly.
     */
    private static final double EPSILON = 0.000001;

    @Test
    public void should_compute_the_total_amount_of_items_as_expected_when_no_offers_are_applied() {
        // Arrange
        Item apple = new Apple(4, 0.20, new NoOffer());
        Item orange = new Orange(3, 0.50, new NoOffer());
        Item waterMelon = new WaterMelon(5, 0.80, new NoOffer());

        Basket basket = new Basket();
        basket.getItems().add(orange);
        basket.getItems().add(apple);
        basket.getItems().add(waterMelon);

        CashDesk cashDesk = new CashDesk(basket);

        // Act
        double result = cashDesk.computeTotal();

        // Assert
        double expected = 6.3; // 4*0.2 + 3*0.5 + 5*0.8
        assertThat(Math.abs(result - expected) < EPSILON, is(true));
    }


    @Test
    public void should_compute_the_total_amount_of_items_as_expected_when_the_offers_are_applied() {
        // Arrange
        Item apple = new Apple(4, 0.20, new BuyOneGetOneFree());
        Item orange = new Orange(3, 0.50, new NoOffer());
        Item waterMelon = new WaterMelon(5, 0.80, new BuyTwoGetOneFree());

        Basket basket = new Basket();
        basket.getItems().add(orange);
        basket.getItems().add(apple);
        basket.getItems().add(waterMelon);

        CashDesk cashDesk = new CashDesk(basket);

        // Act
        double result = cashDesk.computeTotal();

        // Assert
        double expected = 5.1; // 2*0.2 + 3*0.5 + 4*0.8
        assertThat(Math.abs(result - expected) < EPSILON, is(true));
    }


    @Test
    public void should_produce_the_same_result_with_an_extra_watermelon() {
        // Arrange
        Item apple = new Apple(4, 0.20, new BuyOneGetOneFree());
        Item orange = new Orange(3, 0.50, new NoOffer());
        Item waterMelon = new WaterMelon(6, 0.80, new BuyTwoGetOneFree());

        Basket basket = new Basket();
        basket.getItems().add(orange);
        basket.getItems().add(apple);
        basket.getItems().add(waterMelon);

        CashDesk cashDesk = new CashDesk(basket);

        // Act
        double result = cashDesk.computeTotal();

        // Assert
        double expected = 5.1;
        assertThat(abs(result - expected) < EPSILON, is(true));
    }

    @Test
    public void  should_compute_the_total_amount_of_items_as_expected_when_the_offer_buy_two_get_two_free_is_applied() {
        // Arrange
        Item apple = new Apple(4, 0.20, new BuyOneGetOneFree());
        Item orange = new Orange(3, 0.50, new NoOffer());
        Item waterMelon = new WaterMelon(6, 0.80, new BuyTwoGetTwoFree());

        Basket basket = new Basket();
        basket.getItems().add(orange);
        basket.getItems().add(apple);
        basket.getItems().add(waterMelon);

        CashDesk cashDesk = new CashDesk(basket);

        // Act
        double result = cashDesk.computeTotal();

        // Assert
        double expected = 5.1; // 2*0.2 + 3*0.5 + 4*0.8
        assertThat(abs(result - expected) < EPSILON, is(true));

    }

    @Test
    public void should_produce_the_result_when_the_offer_buy_two_get_two_free_is_applied_with_one_more_watermelon() {
        // Arrange
        Item apple = new Apple(4, 0.20, new BuyOneGetOneFree());
        Item orange = new Orange(3, 0.50, new NoOffer());
        Item waterMelon = new WaterMelon(7, 0.80, new BuyTwoGetTwoFree());

        Basket basket = new Basket();
        basket.getItems().add(orange);
        basket.getItems().add(apple);
        basket.getItems().add(waterMelon);

        CashDesk cashDesk = new CashDesk(basket);

        // Act
        double result = cashDesk.computeTotal();

        // Assert
        double expected = 5.1; // 2*0.2 + 3*0.5 + 4*0.8
        assertThat(abs(result - expected) < EPSILON, is(true));
    }
}