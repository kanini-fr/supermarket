package com.recruitment.assessment.model;

import com.recruitment.assessment.offer.Offer;

public class WaterMelon extends Item {
    public WaterMelon() {
        super("WaterMelon");
    }

    public WaterMelon(int quantity, double pricePerUnit, Offer offer) {
        super("WaterMelon", quantity, pricePerUnit, offer);
    }
}
