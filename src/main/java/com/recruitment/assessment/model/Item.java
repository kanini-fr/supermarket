package com.recruitment.assessment.model;

import com.recruitment.assessment.offer.NoOffer;
import com.recruitment.assessment.offer.Offer;

import static java.lang.Math.min;

/**
 * Item representing the items available in the supermarket.
 */
public abstract class Item {

    /**
     * The name of the item
     */
    private String name;
    /**
     * The number of item
     */
    private int quantity;
    /**
     * The price per item
     */
    private double pricePerUnit;
    /**
     * The special offer applied to this item
     */
    private Offer offer;

    /**
     * Creates an item
     *
     * @param name The name of the item
     */
    public Item(String name) {
        this.name = name;
        this.quantity = 0;
        this.pricePerUnit = 0.0;
        this.offer = new NoOffer();
    }

    /**
     * Creates an item
     *
     * @param name         The name of the item
     * @param quantity     The number of item
     * @param pricePerUnit The price per unit
     * @param offer        The special offer applied to this item
     */
    public Item(String name, int quantity, double pricePerUnit, Offer offer) {
        this.name = name;
        this.quantity = quantity;
        this.pricePerUnit = pricePerUnit;
        this.offer = offer;
    }

    /**
     * Compute the price of the items based on the quantity, price per unit and the special offer.
     *
     * @return the price of the items
     */
    public double computePrice() {

        // we must apply euclidean division (a = b*q + r)

        // how many group is eligible for the offer : q
        int q = getQuantity() / (getOffer().getBuyCount() + getOffer().getFreeCount());
        // how many is remaining : r
        int r = getQuantity() % (getOffer().getBuyCount() + getOffer().getFreeCount());
        // among the remainder how many is under buyCount : min
        int min = min(r, getOffer().getBuyCount());

        int effectiveQuantity = getOffer().getBuyCount() * q + min;

        return getPricePerUnit() * effectiveQuantity;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }
}
