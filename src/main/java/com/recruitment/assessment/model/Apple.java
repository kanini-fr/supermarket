package com.recruitment.assessment.model;

import com.recruitment.assessment.offer.Offer;

public class Apple extends Item {
    public Apple() {
        super("Apple");
    }

    public Apple(int quantity, double pricePerUnit, Offer offer) {
        super("Apple", quantity, pricePerUnit, offer);
    }
}
