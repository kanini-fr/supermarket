package com.recruitment.assessment.model;

import com.recruitment.assessment.offer.Offer;

public class Orange extends Item {

    public Orange() {
        super("Orange");
    }

    public Orange(int quantity, double pricePerUnit, Offer offer) {
        super("Orange", quantity, pricePerUnit, offer);
    }
}
