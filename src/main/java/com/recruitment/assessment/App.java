package com.recruitment.assessment;

import com.recruitment.assessment.model.Apple;
import com.recruitment.assessment.model.Item;
import com.recruitment.assessment.model.Orange;
import com.recruitment.assessment.model.WaterMelon;
import com.recruitment.assessment.offer.BuyOneGetOneFree;
import com.recruitment.assessment.offer.BuyTwoGetOneFree;
import com.recruitment.assessment.offer.NoOffer;
import com.recruitment.assessment.service.Basket;
import com.recruitment.assessment.service.CashDesk;

public class App {
    public static void main(String[] args) {

        if (args.length < 3) {
            System.out.println("You must provide the number of item as following : \n" +
                    "\tjava -jar supermarket-1.0-SNAPSHOT.jar <appleCount> <orangeCount> <waterMelonCount>");
            return;
        }

        int appleCount = Integer.parseInt(args[0]);
        int orangeCount = Integer.parseInt(args[1]);
        int waterMelonCount = Integer.parseInt(args[2]);

        Item apple = new Apple(appleCount, 0.20, new BuyOneGetOneFree());
        Item orange = new Orange(orangeCount, 0.50, new NoOffer());
        Item waterMelon = new WaterMelon(waterMelonCount, 0.80, new BuyTwoGetOneFree());

        Basket basket = new Basket();
        basket.getItems().add(orange);
        basket.getItems().add(apple);
        basket.getItems().add(waterMelon);

        CashDesk cashDesk = new CashDesk(basket);

        double result = cashDesk.computeTotal();

        System.out.println(String.format("The total price of provided items is : %s", result));

    }
}
