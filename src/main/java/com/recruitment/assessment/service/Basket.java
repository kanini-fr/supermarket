package com.recruitment.assessment.service;

import com.recruitment.assessment.model.Item;

import java.util.ArrayList;

/**
 * Contains the items of the supermarket
 */
public class Basket {

    private ArrayList<Item> items = new ArrayList<>();

    /**
     * Returns the items of the supermarket
     *
     * @return the items
     */
    public ArrayList<Item> getItems() {
        return items;
    }
}
