package com.recruitment.assessment.service;

import com.recruitment.assessment.model.Item;

/**
 * CashDesk
 */
public class CashDesk {

    private Basket basket;

    public CashDesk(Basket basket) {
        this.basket = basket;
    }

    /**
     * Compute the total price of items contained in the basket
     *
     * @return the basket total price with the special offers if any
     */
    public double computeTotal() {
        return basket.getItems().stream().mapToDouble(Item::computePrice).sum();
    }

}
