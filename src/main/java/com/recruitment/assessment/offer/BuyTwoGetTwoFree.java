package com.recruitment.assessment.offer;

public class BuyTwoGetTwoFree extends Offer {
    public BuyTwoGetTwoFree() {
        super(2, 2);
    }
}
