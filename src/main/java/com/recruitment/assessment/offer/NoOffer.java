package com.recruitment.assessment.offer;

public class NoOffer extends Offer {

    public NoOffer() {
        super(1, 0);
    }
}
