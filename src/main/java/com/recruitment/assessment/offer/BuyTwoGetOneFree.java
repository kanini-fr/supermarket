package com.recruitment.assessment.offer;

public class BuyTwoGetOneFree extends Offer {

    public BuyTwoGetOneFree() {
        super(2, 1);
    }
}
