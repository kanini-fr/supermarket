package com.recruitment.assessment.offer;

public class BuyOneGetOneFree extends Offer {
    public BuyOneGetOneFree() {
        super(1, 1);
    }
}
