package com.recruitment.assessment.offer;

/**
 * Base class representing the special offer available in the supermarket.
 */
public abstract class Offer {

    /**
     * The required number of item to buy in order to get free items
     */
    private int buyCount;
    /**
     * The number of free item offered when the buy count is reached
     */
    private int freeCount;

    public Offer() {
    }

    public Offer(int buyCount, int freeCount) {
        this.buyCount = buyCount;
        this.freeCount = freeCount;
    }

    public int getBuyCount() {
        return buyCount;
    }

    public int getFreeCount() {
        return freeCount;
    }
}
